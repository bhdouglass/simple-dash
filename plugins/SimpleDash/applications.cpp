#include <algorithm>

#include <QDebug>
#include <QDirIterator>
#include <QStandardPaths>
#include <QCoreApplication>

#include "applications.h"
#include "app.h"

/*
TODO

make a list of all apps and add a "selected" property to each app
make a list of all selected apps
*/

bool compareApp(App *a, App *b) {
    return (a->name().compare(b->name(), Qt::CaseInsensitive) < 0);
}

Applications::Applications() {
    m_dashOverride = QStringLiteral("%1/upstart/unity8-dash.override").arg(
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
    );

    refresh();

    m_isDashSet = getIsDashSet();
    Q_EMIT isDashSetChanged();
}

void Applications::refresh() {
    m_applications.clear();

    loadDesktopFiles(DESKTOP_FILES_FOLDER_SYSTEM);
    loadDesktopFiles(DESKTOP_FILES_FOLDER_USER);

    std::sort(m_applications.begin(), m_applications.end(), compareApp);

    Q_EMIT listChanged();

    loadSelected();

    m_appsLoaded = true;
}

App* Applications::get(const QString &appId) {
    App *found = nullptr;

    Q_FOREACH(App *app, m_applications) {
        if (app->appId() == appId || app->id() == appId) {
            found = app;
            break;
        }
    }

    return found;
}

void Applications::loadDesktopFiles(const QString &path) {
    QDirIterator dir(path, QDir::Files | QDir::NoDotAndDotDot | QDir::Readable, QDirIterator::Subdirectories);
    QRegExp rx = QRegExp(QStringLiteral("*.desktop"), Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::Wildcard);

    while (dir.hasNext()) {
        dir.next();
        if (rx.exactMatch(dir.fileName())) {
            QString desktopFile = dir.fileInfo().absoluteFilePath();
            App* app = new App(desktopFile);

            if (app->isVisible() && app->id() != qApp->applicationName()) {
                m_applications.append(app);
            }
            else {
                app->deleteLater();
            }
        }
    }
}

QQmlListProperty<App> Applications::list() {
    return QQmlListProperty<App>(this, m_applications);
}

QQmlListProperty<App> Applications::selectedList() {
    return QQmlListProperty<App>(this, m_selectedApplications);
}

void Applications::loadSelected() {
    m_selectedApplications.clear();
    QStringList packageNames = m_settings.value(QStringLiteral("selectedApplications")).toStringList();

    if (packageNames.count() == 0) {
        packageNames << "com.ubuntu.calendar_calendar"
            << "com.ubuntu.camera_camera"
            << "com.ubuntu.gallery_gallery"
            << "com.ubuntu.music_music"
            << "dialer-app"
            << "messaging-app"
            << "morph-browser"
            << "openstore.openstore-team_openstore";
    }

    Q_FOREACH(QString id, packageNames) {
        App *app = get(id);
        m_selectedApplications.append(app);
    }

    Q_FOREACH (App *app, m_applications) {
        app->setSelected(packageNames.contains(app->id()));
    }

    std::sort(m_selectedApplications.begin(), m_selectedApplications.end(), compareApp);


    Q_EMIT selectedListChanged();
}

void Applications::selectApp(const QString &id) {
    QStringList packageNames = m_settings.value(QStringLiteral("selectedApplications")).toStringList();
    packageNames.append(id);

    m_settings.setValue(QStringLiteral("selectedApplications"), QVariant(packageNames));
    m_settings.sync();

    loadSelected();
}

void Applications::removeApp(const QString &id) {
    QStringList packageNames = m_settings.value(QStringLiteral("selectedApplications")).toStringList();
    int index = packageNames.indexOf(id);
    if (index >= 0) {
        packageNames.removeAt(index);

        m_settings.setValue(QStringLiteral("selectedApplications"), QVariant(packageNames));
        m_settings.sync();

        loadSelected();
    }
}

bool Applications::isDashSet() const {
    return m_isDashSet;
}

bool Applications::getIsDashSet() {
    QFile file(m_dashOverride);
    return file.exists();
}

bool Applications::setDash() {
    QFile file(m_dashOverride);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream out(&file);

    QString appDir = qApp->applicationFilePath();
    QRegExp regex(QStringLiteral("%1\/[0-9.]*\/").arg(qApp->applicationName()));
    QString current = QStringLiteral("%1/current/").arg(qApp->applicationName());
    appDir.replace(regex, current);

    out << QStringLiteral("env BINARY=%1").arg(appDir);
    file.close();

    m_isDashSet = true;
    Q_EMIT isDashSetChanged();

    return true;
}

bool Applications::unsetDash() {
    QFile file(m_dashOverride);
    if (!file.remove()) {
        return false;
    }

    m_isDashSet = false;
    Q_EMIT isDashSetChanged();

    return true;
}
