#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "applications.h"
#include "app.h"

void SimpleDashPlugin::registerTypes(const char *uri) {
    //@uri SimpleDash
    qmlRegisterSingletonType<Applications>(uri, 1, 0, "Applications", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Applications; });
    qmlRegisterType<App>(uri, 1, 0, "App");
}
