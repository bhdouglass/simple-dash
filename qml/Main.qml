import QtQuick 2.4
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'simple-dash.bhdouglass'
    automaticOrientation: true

    property var appWidth: units.gu(10)
    property var appHeight: appWidth + units.gu(2)

    Settings {
        id: settings

        property string password
    }

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: pageStack

        Component.onCompleted: push(Qt.resolvedUrl('AppsPage.qml'))
    }
}
