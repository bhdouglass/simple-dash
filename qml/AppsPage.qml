import QtQuick 2.4
import QtQuick.Layouts 1.1
import AccountsService 0.1
import GSettings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import SimpleDash 1.0
import "Components"

Page {
    id: appsPage

    property int columns: appsPage.width / (appWidth + grid.columnSpacing)
    property int rows: appsPage.height / (appHeight + grid.rowSpacing)

    GSettings {
        id: systemSettings
        schema.id: 'com.ubuntu.touch.system-settings'
    }

    Component.onCompleted: {
        console.log('grid columns', columns, 'grid rows', rows, (appHeight + grid.rowSpacing), (appHeight + grid.rowSpacing) * rows, appsPage.height)
    }

    header: PageHeader {
        id: header
        title: i18n.tr("Apps")
        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Settings')
                    iconName: 'settings'

                    onTriggered: {
                        if (settings.password) {
                            PopupUtils.open(settingsPassword, appsPage);
                        }
                        else {
                            pageStack.push(Qt.resolvedUrl('SettingsPage.qml'));
                        }
                    }
                }
            ]
        }
    }

    Rectangle {
        anchors.fill: grid
        color: systemSettings.dashBackground ? 'black' : 'white'

        Image {
            anchors.fill: parent

            visible: systemSettings.dashBackground
            source: AccountsService.backgroundFile
            fillMode: Image.PreserveAspectCrop
            opacity: systemSettings.backgroundOpacity
        }
    }

    // TODO make this flickable, but only when the list would go off the screen
    GridLayout {
        id: grid

        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            margins: units.gu(1)
        }

        height: rows * (appHeight + rowSpacing)

        columns: appsPage.columns
        columnSpacing: units.gu(1)
        rowSpacing: units.gu(2)

        Repeater {
            model: Applications.selectedList

            delegate: Launcher {
                Layout.fillWidth: true
                Layout.preferredHeight: appHeight
                Layout.maximumWidth: appWidth

                app: model
                color: systemSettings.dashBackground ? 'white' : 'black'

                onClicked: Qt.openUrlExternally(model.uri);
            }
        }
    }

    Component {
        id: settingsPassword

        Dialog {
            id: settingsPasswordDialog

            Label {
                text: i18n.tr('Unlock settings')
                horizontalAlignment: Label.AlignHCenter
                textSize: Label.Large
            }

            function checkPassword() {
                if (password.text == settings.password) {
                    password.focus = false;
                    pageStack.push(Qt.resolvedUrl('SettingsPage.qml'));
                    PopupUtils.close(settingsPasswordDialog);
                }
                else {
                    // TODO error message
                }
            }

            TextField {
                id: password

                echoMode: TextInput.Password
                onAccepted: checkPassword()
            }

            Button {
                text: i18n.tr('Unlock')
                color: UbuntuColors.green
                onClicked: checkPassword()
            }

            Button {
                text: i18n.tr('Cancel')
                onClicked: PopupUtils.close(settingsPasswordDialog)
            }
        }
    }
}
